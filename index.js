require('console-ten').init(console);
var parser = require('fast-xml-parser');
var url = require("url");
var path = require("path");
var fs = require('fs');
var http = require('http');
var reqP = require('request');
var port = 80;

var origin_server;

if (process.env.ORIGINSERVER){
    origin_server = process.env.ORIGINSERVER;
} else {
    origin_server = 'https://dash-cdn1.secure.footprint.net';    
}

var server = http.createServer(function (request, response) {
            
    var body = "";
    request.on('data', function (chunk) {
        body += chunk;
    });

    request.on('end', function () {                                                 
        console.log(request.url);
        reqP.get(origin_server+request.url).on('response', function(res) {
            if (request.url.indexOf('.mpd') >= 0){
                var body = '';
                res.on('data', function (chunk) {
                    body += chunk;
                });
                res.on('end', function () {
                    var contentProtectionTag = findTag(body, "ContentProtection").tagContent;                    
                    var kid = contentProtectionTag.substring(contentProtectionTag.indexOf('default_KID="')+13, contentProtectionTag.indexOf('" xmlns'));
                    var modKid = (kid.substring(0, 8) + "-"+ kid.substring(8, 12) + "-" + kid.substring(12, 16) + "-" + kid.substring(16, 20) + "-" + kid.substring(20, 32));
                    while (body.indexOf(kid) >= 0){
                        body = body.replace(kid, modKid);
                    }                    
                    res.headers['content-length'] = body.length;                                                                
                    response.writeHead(res.statusCode, res.headers);
                    response.write(body);                    
                    response.end();
                });
            } else {                
                res.pipe(response);
            }
        });        
    });            
});


function findTag (xmlString, tagName){    
    var indexOfTag = xmlString.indexOf(tagName);
    var object = {};
    object.tagContent = '';
    object.postContent = '';
    object.prevContent = '';
    if (indexOfTag>=0){
        var prevContent = (xmlString.substring(0,indexOfTag-1));
        var tagContent = xmlString.substring(indexOfTag-1);
        var tagContent = tagContent.substring(0, tagContent.indexOf('>')+1);            

        object.prevContent = prevContent;
        object.tagContent  = tagContent

        xmlString = xmlString.replace(object.prevContent+object.tagContent,'');
        object.postContent  = xmlString;
        return object;
    }
    return null;
}

server.listen(port);    

process.on('uncaughtException', function (err) {
  console.log("[Error] "+err);
})

