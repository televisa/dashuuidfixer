FROM node:7.2.0

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

CMD [ "npm", "start" ]
EXPOSE 80/tcp

# example running:
# docker run --name DashUUIDFixer -it --rm --publish 80:80  blimtv/utils/dashuuidfixer
# docker build -t blimtv/utils/dashuuidfixer .